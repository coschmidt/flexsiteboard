Dear CRAN members,

Thank you for your revision. Please find below my answers to your issues.

> Thanks,
> 
> In your description you have:
>     "split the generated singe page application"
> Should be:
>     "split the generated single page application"
> 
> Also, I believe there is a word missing after:
>     "Put all JavaScript stuff needed in all sections before the first headline with"
> 
> Please always write package names, software names and API (application
> programming interface) names in single quotes in title and description.
> e.g: --> 'JavaScript'
> Please note that package names are case sensitive.

- We have fixed all the mentioned typos, thank you for finding them.

> You write information messages to the console that cannot be easily
> suppressed. -> R/post_knit.R
> It is more R like to generate objects that can be used to extract the
> information a user is interested in, and then print() that object.
> Instead of print()/cat() rather use message()/warning() or
> if(verbose)cat(..) (or maybe stop()) if you really have to write text to
> the console.
> (except for print, summary, interactive functions)

- Sorry, this was a left-over -- we have replaced the `cat` calls by `message`
  calls as suggested.

> Please ensure that your functions do not write by default or in your
> examples/vignettes/tests in the user's home filespace (including the
> package directory and getwd()). This is not allowed by CRAN policies.
> Please omit any default path in writing functions.
> In your examples/vignettes/tests you can write to tempdir().

- `flexsiteboard` does not write any files in the user's home filespace
  except for the generated artifacts, which are necessary for a `rmarkdown` output format.
  All output files are created by `rmarkdown` during its rendering process and written to
  a `rmarkdown` output directory (namely, the figures-directory).
  This means that the user's filespace is modified by rmarkdown,
  and `flexsiteboard` only breaks down `rmarkdown`'s output into several Html files
  to make rendering faster. 

  Nevertheless, we have changed the default behaviour to just fall-back to
  `flexdashboard`, so the `post_knit` is only used, if explicitly requested.
  If it is requested, additionally, we have added code to remove the intermediate `Rmd` files
  from `rmarkdown`'s output directory. All other files are required as they result
  from the `rmarkdown::render` process. These Html files are stored in a subdirectory
  of the `markdown`'s generated output directory (either `output_dir`, if given --
  or in the directory next to `output_file`, if not specified).
   
  If this is a problem with the CRAN policy, please advise me on how to write output
  files of `rmarkdown` document formats; I'll be glad to fix it.

> 
> Please fix and resubmit.
> 
> Best,


## R CMD check results

I've run checks on 

 - macOS `10.15.7 (19H2026)` (`x86_64-apple-darwin17.0, R 4.1.0`), 
 - Debian GNU/Linux `bookworm/sid` (`x86_64-pc-linux-gnu R 4.2.1`)
 - r-hub
 - `check_win_devel` and `check_win_release`

All reported

0 errors | 0 warnings | 1 note

`R-hub Windows Server 2022, R-devel, 64 bit` stated: 
```
  * checking for detritus in the temp directory ... NOTE
  Found the following files/directories:
    'lastMiKTeXException'
```

and `R-hub Fedora Linux, R-devel, clang, gfortran` has:
```
* checking HTML version of manual ... NOTE
Skipping checking HTML validation: no command 'tidy' found
```

I consider both being related with the r-hub infrastructure and unrelated with
my package.

* This is a new release.
